import mysql.connector as mysql
import sys
import re
import traceback 
import os
import requests
from requests import Session
from lxml import etree, html
import socket
import datetime

class DatabaseHelper():

	s = Session()
	
	def log(self,parsename, detail):
			if not os.path.exists(path="log" + "/"):
				os.makedirs(name="log" + "/")

			path = "log" + "/" + parsename +".log"

			if os.path.isfile(path) is not True:
				f= open(path, "w+")
				f.close()

			with open(path, "a") as f:
				f.write("Hora:" + str(datetime.datetime.now()) + "\n")
				f.write(str(detail) + "\n")
				f.write("\n ")

	def __init__(self):
		self.server = "localhost" #186.18.178.206
		self.database = "moneda"
		self.username = "root"
		self.password = "ezedv211201"
	

		if self.password is None:
			self.password = ""

		self.conn = mysql.connect(user=self.username, password=self.password, host=self.server, database=self.database)
		self.cursor = self.conn.cursor(dictionary=True)

	def commit(self):
		self.conn.commit()

	def DBQuery(self, query):
		contador=0
		while True:
			contador+=1
			try:
				self.cursor.execute(query)
				
				if "SELECT" in query or "select" in query:
					
					result = self.cursor.fetchall()
					#self.conn.commit()
					return result
				else:
					#self.conn.commit()
					return True
				break
			except:
				text=traceback.format_exc()+ "\n"
				text+=query+ "\n"
				self.log("DB", text)
				try:
					self.conn = mysql.connect(user=self.username, password=self.password, host=self.server, database=self.database)
					self.cursor = self.conn.cursor(dictionary=True)
				except:
					text=traceback.format_exc() + "\n"
					text+=query+ "\n"
					self.log("DB", text)
				if contador == 3:
					break

		
		return None


	def cerrarConexion(self):
		self.cursor.close()
		self.conn.close()



	def ArreglarFecha(self, date):
		if date == 'null' or date == '-':
			return 'null'
		date=date.replace(' ','')
		listDate = date.split("/")
		return str(listDate[2]) + "/" + str(listDate[1]) + "/" + str(listDate[0])


	def constructorInsert(self, tabla, arrayValores):
		columnas=''
		valores=[]
		query=''
		valoresstring=''
		for valor in arrayValores:
			for (col,val) in valor.items():
				columnas+=col +','
				valores.append(val)
		for value in valores:
			if value is None:
				valoresstring += "null" + ","
				continue

			value = str(value).replace("\n","").replace("  ","").replace("'",'"')
			if value is None or value =="None" or value =="none" or value =="NONE" or value == "S/N" or value == "s/n" or value == "-" or value =="null" or value=="Null" or value =="NULL" or value =='':
				valoresstring += "null" + ","
			elif isinstance(value, int):
				valoresstring += str(value).replace(',','') + ","
			elif len(re.findall(r"[\d]{1,2}/[\d]{1,2}/[\d]{4}", value))>0:
				valoresstring += "'" +self.ArreglarFecha(value)+"',"
			elif(re.match("^[^a-zA-Z]*[^a-zA-Z]$", value)):
				valoresstring += "'" + value.replace('.','').replace(',','.').replace('$','') + "',"
			elif(re.match("^[A-Za-z0-9_-]*$", value)):
				valoresstring += "'" + value + "',"
			else:
				valoresstring +="'"+ value + "',"
		query="INSERT into "+ tabla +"("+columnas[:-1]+") values("+valoresstring[:-1]+") "
		return query
	

	
		
	def historico(self):
		
		
		
		Monedas = [
            {"nombre": "Peso", "codigo": 5, "id_moneda": 1},
            {"nombre": "Real", "codigo": 12, "id_moneda": 2},
            {"nombre": "Dólar estadounidense", "codigo": 2, "id_moneda": 3},
            {"nombre": "Peso Chileno", "codigo": 11, "id_moneda": 4},
        ]
		for moneda in Monedas:
			payload = {
                "Fecha": "2013.1.1",
                "Moneda": moneda['codigo']
            }
			
			r = self.s.post(url="http://www.bcra.gob.ar/PublicacionesEstadisticas/Evolucion_moneda_2.asp", data=payload)
			tree = html.fromstring(r.text)
			filas = tree.xpath("//table/tr")
			for fila in filas:
				arrayValores = []
				arrayValores.append({"fecha": fila[0].text.replace("\r", "").replace("\n", "")})
				arrayValores.append({"nombre": moneda['nombre']})
				arrayValores.append({"id_moneda": moneda["id_moneda"]})
				arrayValores.append({"equivausd": fila[1].text})
				arrayValores.append({"equivapeso": fila[2].text})
				
				self.DBQuery(self.constructorInsert("cotizacion_historico", arrayValores))
				print("Insertado: " + str(fila[0].text.replace("\r", "").replace("\n", "")) + "  " + moneda['nombre'])
		
		self.commit()
		while True:
			try:
				fecha_inicio_consulta = input("Ingrese la fecha de inicio para la consulta (YYYY/MM/DD): ")
				fecha_fin_consulta = input("Ingrese la fecha de fin para la consulta (YYYY/MM/DD): ")
            	# Verificar el formato de las fechas
				datetime.datetime.strptime(fecha_inicio_consulta, "%Y/%m/%d")
				datetime.datetime.strptime(fecha_fin_consulta, "%Y/%m/%d")

            # Realizar consultas según el rango proporcionado por el usuario
				# Realizar consultas según el rango proporcionado por el usuario
				for moneda in Monedas:
					self.consulta_por_rango(
        			fecha_inicio_consulta,
        			fecha_fin_consulta,
        			moneda['nombre']
   					)

				
				continuar = input("¿Desea realizar otra consulta? (S/N): ")
				if continuar.lower() != 's':
					break
			except ValueError:
				print("Error: Formato de fecha incorrecto. Por favor, ingrese las fechas en el formato YYYY/MM/DD.")
			except Exception as e:
				print(f"Error inesperado: {e}")

	def consulta_por_rango(self, fecha_inicio, fecha_fin, tipo_moneda):
		
		# Convertir las fechas a objetos de fech
		fecha_inicio = datetime.datetime.strptime(fecha_inicio, "%Y/%m/%d").date()
		fecha_fin = datetime.datetime.strptime(fecha_fin, "%Y/%m/%d").date()
		# Formatear las fechas para que coincidan con el formato de la base de datos
		fecha_inicio_str = fecha_inicio.strftime("%Y/%m/%d")
		fecha_fin_str = fecha_fin.strftime("%Y/%m/%d")
		# Realizar la consulta en la base de datos
		query = "SELECT fecha, nombre, equivausd, equivapeso FROM cotizacion_historico WHERE fecha BETWEEN '{0}' AND '{1}' AND nombre = '{2}' ORDER BY fecha".format(fecha_inicio_str, fecha_fin_str, tipo_moneda)


		result = self.DBQuery(query)

    
		for row in result:
			print("-" * 50)  # Separador
			print(f"Fecha: {row['fecha']}")
			print(f"Moneda: {row['nombre']}")
			print(f"Equivalente al USD:")
			print(f"{row['equivausd']}")
			print(f"Equivalente al Peso:")
			print(f"Equiv Peso: {row['equivapeso']}")
			
		print("-" * 50)

	def actualizacion(self):
		try:
			self.actualizar_datos()
			print("Datos actualizados exitosamente.")
		except Exception as e:
			print(f"Error durante la actualización: {e}")

	def actualizar_datos(self):
    	# Obtener la última fecha registrada en la base de datos
		query_ultima_fecha = "SELECT MAX(fecha) as ultima_fecha FROM cotizacion_historico"
		result_ultima_fecha = self.DBQuery(query_ultima_fecha)
		ultima_fecha = result_ultima_fecha[0]['ultima_fecha'] if result_ultima_fecha and result_ultima_fecha[0]['ultima_fecha'] else "2013/01/01"

    	# Obtener datos desde la última fecha hasta la fecha actual
		fecha_actual = datetime.datetime.now().strftime("%Y.%m.%d")
		Monedas = [
            {"nombre": "Peso", "codigo": 5, "id_moneda": 1},
            {"nombre": "Real", "codigo": 12, "id_moneda": 2},
            {"nombre": "Dólar estadounidense", "codigo": 2, "id_moneda": 3},
            {"nombre": "Peso Chileno", "codigo": 11, "id_moneda": 4},
        ]
		
		for moneda in Monedas:
			payload = {
            	"Fecha": ultima_fecha,
            	"Moneda": moneda['codigo']
        		}
        
			r = self.s.post(url="http://www.bcra.gob.ar/PublicacionesEstadisticas/Evolucion_moneda_2.asp", data=payload)
			tree = html.fromstring(r.text)
			filas = tree.xpath("//table/tr")
        
			for fila in filas:
				arrayValores = []
				arrayValores.append({"fecha": fila[0].text.replace("\r", "").replace("\n", "")})
				arrayValores.append({"nombre": moneda['nombre']})
				arrayValores.append({"id_moneda": moneda["id_moneda"]})
				arrayValores.append({"equivausd": fila[1].text})
				arrayValores.append({"equivapeso": fila[2].text})

            	# Insertar los nuevos datos en la base de datos
				self.DBQuery(self.constructorInsert("cotizacion_historico", arrayValores))
				print("Insertado: " + str(fila[0].text.replace("\r", "").replace("\n", "")) + "  " + moneda['nombre'])
		
		self.commit()

	
	def consulta_especifica(self, fecha, tipo_moneda):
       
    	# Formatear la fecha para que coincida con el formato de la base de datos
		fecha_str = datetime.datetime.strptime(fecha, "%Y/%m/%d").date().strftime("%Y/%m/%d")

    	# Realizar la consulta en la base de datos
		query = "SELECT equivausd, equivapeso FROM cotizacion_historico WHERE fecha = '{0}' AND nombre = '{1}'".format(fecha_str, tipo_moneda)
		result = self.DBQuery(query)
		if result:
			return result[0]  # Devolver el primer resultado (debería ser único para esa fecha y tipo de moneda)
		else:
			return None  # En caso de que no haya resultados para la fecha y tipo de moneda proporcionados

	def diferencia(self, fecha_inicio, fecha_fin, tipo_moneda):
		try:
			# Convertir las fechas a objetos de fecha
			fecha_inicio = datetime.datetime.strptime(fecha_inicio, "%Y/%m/%d").date()
			fecha_fin = datetime.datetime.strptime(fecha_fin, "%Y/%m/%d").date()

        	# Formatear las fechas para que coincidan con el formato de la base de datos
			fecha_inicio_str = fecha_inicio.strftime("%Y/%m/%d")
			fecha_fin_str = fecha_fin.strftime("%Y/%m/%d")

        	# Obtener los valores de las fechas proporcionadas
			query_inicio = (
            	"SELECT {0} FROM cotizacion_historico WHERE fecha = '{1}' AND nombre = '{2}'"
        	).format("equivausd", fecha_inicio_str, tipo_moneda)
			query_fin = (
            	"SELECT {0} FROM cotizacion_historico WHERE fecha = '{1}' AND nombre = '{2}'"
        	).format("equivausd", fecha_fin_str, tipo_moneda)
			
			result_inicio = self.DBQuery(query_inicio)
			result_fin = self.DBQuery(query_fin)
			# Validar si se obtuvieron resultados para ambas fechas
			if not result_inicio or not result_fin:
				print("No se encontraron datos para las fechas proporcionadas.")
				return

        	# Obtener los valores de las consultas
			valor_inicio = float(result_inicio[0]["equivausd"])
			valor_fin = float(result_fin[0]["equivausd"])
			
			# Calcular la diferencia porcentual
			diferencia = ((valor_fin - valor_inicio) / valor_inicio) * 100
			print(f"Diferencia porcentual entre {fecha_inicio_str} y {fecha_fin_str}: {diferencia:.2f}")
		except Exception as e:
			print(f"Error en la función de diferencia: {e}")

	def servidor(self):
        # Configurar el servidor en el puerto 2345
		servidor_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		servidor_socket.bind(('localhost', 2345))
		servidor_socket.listen(1)
		print("Esperando conexiones en el puerto 2345...")
		while True:
			# Esperar a que se conecte un cliente
			cliente_socket, direccion = servidor_socket.accept()
			print("Conexión establecida desde", direccion)
			
			try:
                # Recibir la fecha del client
				fecha_cliente = cliente_socket.recv(1024).decode('utf-8')

                # Validar el formato de la fecha
				datetime.datetime.strptime(fecha_cliente, "%Y/%m/%d")

                # Realizar la consulta en la base de datos para obtener el valor del dólar estadounidense
				query = "SELECT equivausd FROM cotizacion_historico WHERE fecha = '{0}' AND nombre = 'Dólar estadounidense'".format(fecha_cliente)
				result = self.DBQuery(query)
				if result:
					valor_dolar = result[0]['equivausd']
					respuesta = f"El valor del dólar estadounidense en la fecha {fecha_cliente} es: {valor_dolar}"
				else:
					respuesta = "No hay datos para la fecha proporcionada."

                # Enviar la respuesta al cliente
				cliente_socket.send(respuesta.encode('utf-8'))
			
			except ValueError:
				respuesta = "Error: Formato de fecha incorrecto. Por favor, ingrese la fecha en el formato YYYY/MM/DD."
				cliente_socket.send(respuesta.encode('utf-8'))
			except Exception as e:
				respuesta = f"Error inesperado: {e}"
				cliente_socket.send(respuesta.encode('utf-8'))

            # Cerrar la conexión con el cliente
		cliente_socket.close()

        	# Cerrar el servidor
		servidor_socket.close()

	
	def menu(self):
		while True:
			print("1. Histórico")
			print("2. Actualización")
			print("3. Consulta específica")
			print("4. Consulta de moneda por rango de fechas")
			print("5. Diferencia")
			print("6. Servidor")
			print("7. Salir")
			choice = input("Ingrese el número de la opción deseada: ")
			if choice == '1':
				self.historico()
			elif choice == '2':
				self.actualizacion()
			elif choice == '3':
				fecha_consulta = input("Ingresa una fecha desde el 2023/01/01 en adelante, con el formato 2023/02/01: " )
				tipo_moneda_consulta = input("Ingresa el nombre de la moneda: ")
				resultado = self.consulta_especifica(fecha_consulta, tipo_moneda_consulta)
				if resultado:
					print(f"fecha:{fecha_consulta}")
					print(f"moneda:{tipo_moneda_consulta}")
					print(f"Equivalente al USD:")
					print(f"{resultado['equivausd']}")
					print(f"Equivalente al peso:")
					print(f"{resultado['equivapeso']}")
				else:
					print(f"No hay datos para la fecha {fecha_consulta} y la moneda {tipo_moneda_consulta}")
			elif choice == '4':
				fecha_inicio_consulta = input("Ingrese la fecha de inicio para la consulta (YYYY/MM/DD): ")
				fecha_fin_consulta = input("Ingrese la fecha de fin para la consulta (YYYY/MM/DD): ")
				moneda = input("Ingrese el nombre de la moneda:")
				datetime.datetime.strptime(fecha_inicio_consulta, "%Y/%m/%d")
				datetime.datetime.strptime(fecha_fin_consulta, "%Y/%m/%d")
				self.consulta_por_rango(
        			fecha_inicio_consulta,
        			fecha_fin_consulta,
        			moneda)
			elif choice == '5':
				fecha_inicio_consulta = input("Ingrese la fecha de inicio para la consulta (YYYY/MM/DD): ")
				fecha_fin_consulta = input("Ingrese la fecha de fin para la consulta (YYYY/MM/DD): ")
				moneda = input("Ingrese el nombre de la moneda:")
				datetime.datetime.strptime(fecha_inicio_consulta, "%Y/%m/%d")
				datetime.datetime.strptime(fecha_fin_consulta, "%Y/%m/%d")
				self.diferencia(fecha_inicio_consulta,
        			fecha_fin_consulta,
        			moneda)
			elif choice == '6':
				self.servidor()
			elif choice == "7":
				break
			else:
				print("Opción no válida. Por favor, elija una opción válida.")