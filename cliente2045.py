import socket

def cliente():
    # Configurar el cliente
    cliente_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    cliente_socket.connect(('localhost', 2345))

    try:
        # Solicitar la fecha al usuario
        fecha = input("Ingrese la fecha (YYYY/MM/DD): ")

        # Enviar la fecha al servidor
        cliente_socket.send(fecha.encode('utf-8'))

        # Recibir la respuesta del servidor
        respuesta = cliente_socket.recv(1024).decode('utf-8')

        # Mostrar la respuesta al usuario
        print(respuesta)

    except ValueError:
        print("Error: Formato de fecha incorrecto. Por favor, ingrese la fecha en el formato YYYY/MM/DD.")
    except Exception as e:
        print(f"Error inesperado: {e}")

    # Cerrar la conexión con el servidor
    cliente_socket.close()

# Ejecutar el cliente
cliente()
