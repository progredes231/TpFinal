Trabajo práctico Integrador

CONSIGNAS:

Se debe realizar un programa en python utilizando POO y la librería request  que guarde en una tabla los datos de las monedas Peso, Peso chileno Real y Dolar estadounidense en todas sus cotizaciones, el tipo de moneda debe ir en otra tabla y ambas deben estar asociada por id_moneda.
Menú: Debe contener un método de menú para poder utilizar todos los métodos del programa


Métodos

Debe tener un método que se llame histórico al que se le pasa un rango de fechas para levantar el histórico, el histórico es desde 2013.
Método de actualización que debe levantar desde la última fecha que se corrió hasta el momento en donde se corra.
Método de consulta específica: Se le debe pasar como parámetro la fecha y el tipo de moneda y debe devolver el valor
Método de consulta por rango: Se le pasa como parámetro la fecha de inicio, la fecha de fin y el tipo de moneda en ese orden y debe devolver una lista con los valores 
Método diferencia: Se le pasa como parámetro la fecha de inicio, la fecha de fin y el tipo de moneda en ese orden y debe devolver la diferencia entre los valores de esas dos fechas. ej : +5.63 o -2.52 siendo la diferencia porcentual. 
Método Servidor: Al ejecutar este método el programa ejecuta un servidor de sockets en el puerto 2345 que cuando se conecte un cliente debe recibir una fecha y debe devolver el valor de la moneda dólar estadounidense.

Se debe realizar un programa cliente que permita mostrar el uso del método servidor el proyecto, ingresando la fecha por teclado indicando por pantalla el formato. 

Todo el código debe estar debidamente documentado dentro del código,
Se debe subir al repositorio con un readme.md indicando su uso, librerias e instalación y autoría.


DESARROLLO: 

Mi trabajo práctico se compone de 3 archivos .py, los cuales son: DataBaseHelper.py, mainTp.py y cliente2045.py. Y ademas una Base de datos llamada moneda.sql.

Los métodos que se pidieron en las consignas del trabajo práctico se encuentran todos en mi DataBaseHelper.py y la ejecución de los archivos la realice en el archivo mainTp.py. 

Métodos agregados: 

--------------------------------------------------------.-----------------------------------------------------------------------
EN ARCHIVO DataBaseHelper.py:

historico(self): Esta función cuenta con una variable moneda que contiene una lista con un diccionario que contienen el nombre, el código y el id de la monedas. Con un ciclo for se hace una iterración entre cada moneda.
Para cada moneda, crea un diccionario payload con una fecha inicial fija ("2013.1.1") y el código de la moneda.
Realiza una solicitud POST a una URL ("http://www.bcra.gob.ar/PublicacionesEstadisticas/Evolucion_moneda_2.asp") con el payload.
Parsea la respuesta HTML de la página web utilizando la biblioteca lxml. Extrae datos de una tabla en la página web, como la fecha, el nombre de la moneda, el identificador de la moneda, el valor equivalente en dólares (equivausd), y el valor equivalente en pesos (equivapeso).Construye un diccionario llamado arrayValores con estos datos. Llama al método DBQuery para insertar los datos en la base de datos utilizando el método constructorInsert. Esto agrega los datos recopilados en la tabla "cotizacion_historico". Imprime en la consola un mensaje indicando que se ha insertado un conjunto de datos.Realiza un commit en la base de datos utilizando el método commit.
Después de cargar los registros en la BD, entra en un bucle while true. Solicita al usuario que ingrese las fechas de inicio y fin para una consulta específica en formato "YYYY/MM/DD".Realiza validación de formato para las fechas ingresadas.Itera sobre cada moneda en la lista Monedas y llama al metodo consulta_por_rango() El cual termina imprimiendo los datos del rango de fechas ingresadas y de todas las monedas.

consulta_por_rango(self, fecha_inicio, fecha_fin, tipo_moneda) al cual le pasa las fechas y el nombre de la moneda. Este método convierte las fechas de inicio y fin, que están en formato "YYYY/MM/DD", en objetos de fecha utilizando la función strptime de la clase datetime.datetime luego formatea las fechas convertidas para que coincidan con el formato de la base de datos utilizando la función strftime.Después hac una consulta SQL utilizando las fechas formateadas y el tipo de moneda proporcionados.La consulta busca registros en la tabla "cotizacion_historico" donde la fecha esté entre la fecha de inicio y fin, y el nombre de la moneda sea igual al tipo de moneda proporcionado. Ordena los resultados por la fecha en orden ascendente.
Itera sobre los resultados de la consulta y muestra la información en la consola para cada fila encontrada con el formato configuarado.

Actualizacion(self) Esta función intenta ejecutar la función actualizar_datos() para realizar la actualización de datos y si ocurre alguna excepción durante la actualización, captura la excepción y muestra un mensaje de error indicando que hubo un problema durante la actualización.

actualizar_datos(self) Realiza una consulta SQL para obtener la fecha más reciente registrada en la tabla cotizacion_historico utilizando la función MAX(fecha).Guarda la última fecha encontrada o establece una fecha de inicio predeterminada ("2013/01/01") si no hay registros en la base de datos.
Define una lista de monedas llamada Monedas, donde cada moneda tiene su nombre, código e identificador.Itera sobre cada moneda en la lista Monedas
Para cada moneda, crea un payload con la última fecha registrada y el código de la moneda.Realiza una solicitud HTTP POST a una URL externa (http://www.bcra.gob.ar/PublicacionesEstadisticas/Evolucion_moneda_2.asp) con el payload para obtener información de cotizaciones.
Utiliza XPath para extraer la información relevante de la respuesta HTML, como la fecha, el nombre de la moneda, el id, el equivalente en USD y el equivalente en peso. Para cada fila obtenida, construye un array de valores y utiliza el método constructorInsert para crear una consulta SQL de inserción.Ejecuta la consulta en la base de datos mediante DBQuery para insertar los nuevos datos en la tabla cotizacion_historico.
Realiza un commit en la base de datos para aplicar los cambios.
Imprime en la consola un mensaje indicando que se ha insertado correctamente la información de cotización.
En resumen, estas funciones juntas permiten actualizar la base de datos con información de cotizaciones de monedas, partiendo de la última fecha registrada en la base de datos y obteniendo datos de la URL del BCRA.

consulta_especifica(self, fecha, tipo_moneda) tiene como objetivo realizar una consulta específica en la base de datos para obtener la información de cotización de una sola moneda en una fecha especifica. La fecha proporcionada como argumento se convierte a un objeto de fecha utilizando datetime.datetime.strptime(fecha, "%Y/%m/%d").date(). Luego, se formatea nuevamente como una cadena en el formato compatible con la base de datos usando .strftime("%Y/%m/%d"). Esto garantiza que el formato de la fecha coincida con el formato almacenado en la base de datos.Construye una consulta SQL utilizando la fecha formateada y el tipo de moneda proporcionados.La consulta busca en la tabla cotizacion_historico los valores equivausd y equivapeso donde la fecha es igual a la fecha proporcionada y el nombre de la moneda es igual al tipo de moneda proporcionado.Ejecuta la consulta en la base de datos mediante DBQuery.Verifica si la consulta devuelve algún resultado.Si hay resultados, devuelve el primer resultado (se asume que debería ser único para esa fecha y tipo de moneda).Si no hay resultados, devuelve None.


diferencia(self, fecha_inicio, fecha_fin, tipo_moneda) tiene como objetivo calcular y mostrar la diferencia porcentual entre los valores de cotización de una moneda para dos fechas diferentes.
Las fechas proporcionadas como argumentos se convierten en obj. tipo fechas. Las fechas convertidas se formatean nuevamente como cadenas en el formato compatible con la base de datos. Se construyen dos consultas SQL para obtener el valor de equivausd para las fechas de inicio y fin especificadas y el tipo de moneda proporcionado.Se ejecutan estas consultas en la base de datos mediante DBQuery. Se obtienen los valores de equivausd de las consultas para las fechas de inicio y fin. Se calcula la diferencia porcentual y el resultado se imprime por consola.

servidor(self) implementa un servidor de sockets que escucha conexiones en el puerto 2345 y espera a que un cliente se conecte.
Se crea un objeto de socket utilizando socket.socket(socket.AF_INET, socket.SOCK_STREAM). Este objeto representa el socket del servidor.
Se realiza el enlace (bind) del socket a la dirección ('localhost', 2345). Esto significa que el servidor estará escuchando en el puerto 2345. Se utiliza el método listen(1) para poner el socket del servidor en modo de escucha, permitiendo hasta 1 conexión en espera.Se imprime un mensaje indicando que el servidor está esperando conexiones. Se inicia un bucle infinito (while True) para esperar conexiones de clientes continuamente.
Cuando un cliente se conecta, se acepta la conexión utilizando accept(), y se obtiene un nuevo socket del cliente (cliente_socket) y la dirección del cliente (direccion). Se imprime un mensaje indicando que se ha establecido una conexión con el cliente. Se intenta recibir datos del cliente utilizando cliente_socket.recv(1024).decode('utf-8'). Estos datos se esperan en formato UTF-8 y representan la fecha que el cliente está solicitando. Se utiliza datetime.datetime.strptime(fecha_cliente, "%Y/%m/%d") para validar que la fecha tenga el formato correcto. e construye una consulta SQL para obtener el valor de equivausd de la moneda "Dólar estadounidense" para la fecha proporcionada por el cliente.
La consulta se ejecuta utilizando DBQuery.Si hay resultados, se obtiene el valor del dólar estadounidense y se construye una respuesta. Si no hay resultados, se indica que no hay datos para la fecha proporcionada. Y despues se cierran las conexiones del cliente y del servidor.

menu(self) Despliega un menú interactivo donde imprime las opciones y te proporciona un input para que selecciones la opción.
Es un while true que te muestras las siguientes opciones Se imprime un menú con las siguientes opciones:
1. Histórico
2. Actualización
3. Consulta específica
4. Consulta de moneda por rango de fechas
5. Diferencia
6. Servidor
7. Salir
Se solicita al usuario que ingrese el número correspondiente a la opción deseado.
Se utiliza una serie de declaraciones if-elif-else para manejar la opción seleccionada por el usuario.
Si la opción es 1, se llama a self.historico().
Si la opción es 2, se llama a self.actualizacion().
Si la opción es 3, se solicita al usuario ingresar una fecha y el nombre de la moneda para realizar una consulta específica (self.consulta_especifica), y se muestra el resultado si hay datos disponibles.
Si la opción es 4, se solicita al usuario ingresar fechas y el nombre de la moneda para realizar una consulta por rango de fechas (self.consulta_por_rango).
Si la opción es 5, se solicita al usuario ingresar fechas y el nombre de la moneda para calcular y mostrar la diferencia porcentual (self.diferencia).
Si la opción es 6, se llama a self.servidor().
Si la opción es 7, se sale del bucle (break).
Si la opción no es válida, se imprime un mensaje indicando que la opción no es válida y se vuelve a mostrar el menú.


----------------------------------------------------------.-------------------------------------------------------------------

EN ARCHIVO mainTp.py:

Se importa el archivo DatabaaHelper.py con las class DabaseHelper. Se instancia la clase, Y se hace un if __name__ == '__main__': donde se ejecuta la instacia con el método menu() el cual cumple con su funcion.

----------------------------------------------------------.-------------------------------------------------------------------

EN ARCHIVO cliente2045.py:

Aqui se importa el socket. Se define la funcion llamada cliente. cliente_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM): Crea un objeto de socket para el cliente. AF_INET indica que se usará IPv4 y SOCK_STREAM indica que se utilizará un socket de flujo para la comunicación TCP. cliente_socket.connect(('localhost', 2345)): Conecta el cliente al servidor en el host localhost (la misma máquina) y en el puerto 2345. Se le solicita una fecha al usuario mediante un input. Y recibe la respuesta del servidor, esta se muestra en la consola.

En resumen, este código define un cliente de sockets que se conecta a un servidor en el puerto 2345 en el mismo host (localhost). El cliente solicita al usuario ingresar una fecha, la envía al servidor y muestra la respuesta recibida del servidor. Además, maneja posibles errores, como un formato de fecha incorrecto o errores inesperados durante la ejecución.


